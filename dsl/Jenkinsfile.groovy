job('CRA DSL') {
    scm {
        git {
            remote {
                url('https://moonman96@bitbucket.org/moonman96/jenkins.git') 
            }
            branch('master')

            extensions {
            localBranch 'master'
            }
        }
    }

    triggers {
        scm('H/5 * * * *')
    }

    wrappers {
        nodejs('node1')
    }

    steps {
        shell("npm install")
    }
}