job('CRA DSL') {

    scm {
        git {
            remote {
                url('https://moonman96@bitbucket.org/moonman96/jenkins.git') 
            }
            branch('master')

            extensions {
            localBranch 'master'
            }
        }
    }

    triggers {
        scm('H/5 * * * *')
    }

    wrappers {
        nodejs('node1')
    }

    steps {
        dockerBuildAndPublish {
            repositoryName('arifullah/docker-react-dsl')
            tag('${GIT_REVISION,length=9}')
            registryCredentials('dockerhub')
            forcePull(false)
            forceTag(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}